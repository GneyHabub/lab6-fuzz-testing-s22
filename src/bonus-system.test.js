import { calculateBonuses } from "./bonus-system.js";

const prec = 3

describe('Bonus system tests', () => {
    test('Standard', (done) => {
        const program = 'Standard';
        expect(calculateBonuses(program, 7500)).toBeCloseTo(0.05, prec);
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.075, prec);
        expect(calculateBonuses(program, 12000)).toBeCloseTo(0.075, prec);
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.1, prec);
        expect(calculateBonuses(program, 51000)).toBeCloseTo(0.1, prec);
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.125, prec);
        expect(calculateBonuses(program, 500000)).toBeCloseTo(0.125, prec);
        done();
    });

    test('Premium', (done) => {
        const program = 'Premium';
        expect(calculateBonuses(program, 7500)).toBeCloseTo(0.1, prec);
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.15, prec);
        expect(calculateBonuses(program, 12000)).toBeCloseTo(0.15, prec);
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.2, prec);
        expect(calculateBonuses(program, 51000)).toBeCloseTo(0.2, prec);
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.25, prec);
        expect(calculateBonuses(program, 500000)).toBeCloseTo(0.25, prec);
        done();
    });

    test('Diamond', (done) => {
        const program = 'Diamond';
        expect(calculateBonuses(program, 7500)).toBeCloseTo(0.2, prec);
        expect(calculateBonuses(program, 12000)).toBeCloseTo(0.3, prec);
        expect(calculateBonuses(program, 51000)).toBeCloseTo(0.4, prec);
        expect(calculateBonuses(program, 500000)).toBeCloseTo(0.5, prec);
        done();
    });


    test('Invalid', (done) => {
        const program = 'Invalid';
        expect(calculateBonuses(program, 7500)).toEqual(0);
        expect(calculateBonuses(program, 12000)).toEqual(0);
        expect(calculateBonuses(program, 51000)).toEqual(0);
        expect(calculateBonuses(program, 500000)).toEqual(0);
        done();
    });
});